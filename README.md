# The Alexandrian Collective #



### What is The Alexandrian Collective? ###

**The Alexandrian Collective (AC)** is a non-profit dedicated to the preservation and pursuit of knowledge
in science and the arts.